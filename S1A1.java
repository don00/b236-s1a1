
package com.zuitt.example.Activity;
import java.util.Scanner;

public class S1A1 {
    public static void main(String [] args){
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;
        Scanner scanner = new Scanner(System.in);
     
        System.out.println("Please enter you First Name: ");
        firstName = scanner.nextLine();
        
        System.out.println("Last Name: ");
        lastName = scanner.nextLine();
        
        System.out.println("Please enter your grade for the first subject: ");
        firstSubject = scanner.nextDouble();
        
        System.out.println("second subject: ");
        secondSubject = scanner.nextDouble();
        
        System.out.println("third subject:");
        thirdSubject = scanner.nextDouble();
        double average  = (firstSubject + secondSubject + thirdSubject)/3;
        
        System.out.println(average);
        System.out.println("Good day " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + String.format("%.2f", average) );
    }
}
